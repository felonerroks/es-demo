package com.genyon.demo.config;

import org.elasticsearch.xpack.sql.jdbc.EsDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class ElasticsearchJdbcConfig {
    @Value("${es.jdbc.address}")
    private String address;

    @Bean(name = "esConnection")
    public Connection connection() throws SQLException {
        EsDataSource dataSource = new EsDataSource();
        dataSource.setUrl(address);
        return dataSource.getConnection();
    }
}
