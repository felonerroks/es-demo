package com.genyon.demo;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.core.GetSourceRequest;
import org.elasticsearch.client.core.GetSourceResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.ScrollableHitSource;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Slf4j
class EsDemoApplicationTests {

    /**
     * es 的 low-level-client
     * low level client 在8.0版本将被弃用
     */
    @Autowired
    RestClient restClient;

    /**
     * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-getting-started.html
     * es 的 high-level-client
     */
    @Autowired
    RestHighLevelClient restHighLevelClient;

    // jdbc 获取 es 连接
    @Autowired
    @Qualifier("esConnection")
    Connection connection;

    /**
     * low level
     * 全局查询 _search 下的数据，不限定 index
     *
     * @throws IOException
     */
    @Test
    void esRequest() throws IOException {
        Request request = new Request(
                "GET",
                "/_search"
        );

        Response response = restClient.performRequest(request);
        log.info(JSONUtil.toJsonStr(response));
        // 获得数据对象
        log.info(JSONUtil.toJsonStr(response.getEntity().getContent()));
        int statusCode = response.getStatusLine().getStatusCode();
        log.info("" + statusCode);
    }

    /**
     * 查询 index 是否存在
     *
     * @throws IOException
     */
    @Test
    void highLevelTest() throws IOException {
        // 对象初始化时指定目标 index
        GetIndexRequest request = new GetIndexRequest("test_idx");


        request.local(false);
        request.humanReadable(true);
        request.includeDefaults(false);
        request.indicesOptions(IndicesOptions.lenientExpandOpen());
        boolean b = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        log.info("" + b);
    }

    /**
     * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-search.html
     * @throws IOException
     */
    @Test
    void queryTest() throws IOException {
        // 对象初始化时指定目标 index，可指定多个，不指定 index 则全库搜索。
        SearchRequest searchRequest = new SearchRequest("test_idx", "index2");

        // IndicesOptions 控制遇到不可用的 index 时处理的方式。
        searchRequest.indicesOptions(IndicesOptions.lenientExpandOpen());
        // 一般请求参数通过 SearchSourceBuilder 传入
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        // 特殊 query
        // searchSourceBuilder.query(QueryBuilders.termQuery("user", "kimchy"));
        // 搜索结果的起始索引
        searchSourceBuilder.from(0);
        // 返回的结果集大小
        searchSourceBuilder.size(5);
        // 超时参数
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        // 将 builder 设为搜索参数
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits.getHits()) {
            log.info(JSONUtil.toJsonStr(hit.getSourceAsMap()));
        }
        log.info(JSONUtil.toJsonStr(searchResponse.getHits().getAt(0)));
    }

    /**
     * ES 的 index 操作
     * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-document-index.html
     *
     * @throws IOException
     */
    @Test
    void indexTest() throws IOException {
        // 对象初始化时指定目标 index
        IndexRequest request = new IndexRequest("index_test");
        // 用于请求的 json 对象。可以通过工具将类对象转化为 json 对象。
        String jsonString = "{" +
                "\"user\":\"kimchy\"," +
                "\"postDate\":\"2013-01-30\"," +
                "\"message\":\"trying out Elasticsearch\"" +
                "}";

        request.source(jsonString, XContentType.JSON);
        /**
         * 或采用如下形式
         * request.source(
         * "key1",new Object(),
         * "key2","obj2");
         */

        // 设置 routing
        request.routing("routing");
        // 设置超时
        request.timeout(TimeValue.timeValueSeconds(1));
        // request.timeout("1s");

        // 异步请求 Listener
        //ActionListener listener = new ActionListener<IndexResponse>() {
        //    @Override
        //    public void onResponse(IndexResponse indexResponse) {

        //    }

        //    @Override
        //    public void onFailure(Exception e) {

        //    }
        //};
        // 异步请求
        // restHighLevelClient.indexAsync(request, RequestOptions.DEFAULT, listener);

        // 下列请求为同步请求
        IndexResponse indexResponse = restHighLevelClient.index(request, RequestOptions.DEFAULT);

        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
            // 请求时 index 不存在，创建 index
        } else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
            // 更新 index 内容
        }
    }

    // 使用 map 类，作为 index 请求参数。
    @Test
    void indexMapTest() throws IOException {
        IndexRequest request = new IndexRequest("index_test");
        Map<String, String> data = new HashMap<>();
        data.put("user", "map_user");
        data.put("postDate", "2020-01-30");
        data.put("message", "es map index msg");
        request.id("1");
        request.source(data, XContentType.JSON);

        IndexResponse indexResponse = restHighLevelClient.index(request, RequestOptions.DEFAULT);
        log.info("" + indexResponse.getResult().getOp());
        log.info(JSONUtil.toJsonStr(indexResponse.getIndex()));
    }

    /**
     * ES get API 使用
     * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-document-get.html
     */
    @Test
    void getTest() throws IOException {
        GetRequest getRequest = new GetRequest(
                // 指定 index, 此处指定了不存在的 index，下面 try-catch 块会进入异常捕捉
                "KKK",
                // 指定查询对象的 id
                "1");

        try {
            GetResponse getResponse = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
            if (getResponse.isExists()) {
                log.info(JSONUtil.toJsonStr(getResponse.getId()));
            }
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                log.info(e.getDetailedMessage());
            }
        }
    }

    /**
     * ES get API 正常使用
     *
     * @throws IOException
     */
    @Test
    void getSourceTest() throws IOException {
        GetSourceRequest getSourceRequest = new GetSourceRequest(
                "index_test",
                "1");

        // 可选配置
        // 包含的字段名
        // String[] includes = Strings.EMPTY_ARRAY;
        String[] includes = new String[]{"message"};
        // 排除的字段名
        String[] excludes = new String[]{"postDate"};
        getSourceRequest.fetchSourceContext(
                new FetchSourceContext(true, includes, excludes));

        GetSourceResponse getResponse = restHighLevelClient.getSource(getSourceRequest, RequestOptions.DEFAULT);
        if (!getResponse.getSource().isEmpty()) {
            Map<String, Object> source = getResponse.getSource();
            log.info(JSONUtil.toJsonStr(source));
        }
    }

    @Test
    void deleteTest() throws IOException {
        IndexRequest request = new IndexRequest("index_test");
        Map<String, String> data = new HashMap<>();
        data.put("user", "map_user");
        data.put("postDate", "2020-01-30");
        data.put("message", "es map index msg");
        request.id("delete_id");

        request.source(data, XContentType.JSON);
        IndexResponse indexResponse = restHighLevelClient.index(request, RequestOptions.DEFAULT);

        GetRequest getRequest = new GetRequest(
                "index_test",
                "delete_id");
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        getRequest.storedFields("_none_");
        boolean exists = restHighLevelClient.exists(getRequest, RequestOptions.DEFAULT);
        assertTrue(exists);

        DeleteRequest deleterequest = new DeleteRequest(
                "index_test",
                "delete_id");
        DeleteResponse deleteResponse = restHighLevelClient.delete(deleterequest, RequestOptions.DEFAULT);
        // 检测删除结果状态
        assertEquals(DocWriteResponse.Result.DELETED, deleteResponse.getResult());

    }

    @Test
    void updateTest() throws IOException {
        IndexRequest request = new IndexRequest("index_test");
        Map<String, String> data = new HashMap<>();
        data.put("user", "map_user");
        data.put("postDate", "2020-01-30");
        data.put("message", "es map index msg");
        request.id("update_id");

        request.source(data, XContentType.JSON);
        IndexResponse indexResponse = restHighLevelClient.index(request, RequestOptions.DEFAULT);
        log.info("+" + JSONUtil.toJsonStr(indexResponse.getResult()));

        GetRequest getRequest = new GetRequest(
                "index_test",
                "update_id");
        JSONObject jsonObject;
        try {
            GetResponse getResponse = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
            if (getResponse.isExists()) {
                log.info(JSONUtil.toJsonStr(getResponse.getId()));
                jsonObject = JSONUtil.parseObj(getResponse.getSourceAsString());
            }
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                log.info(e.getDetailedMessage());
            }
        }

        UpdateRequest updateRequest = new UpdateRequest("index_test", "update_id");
        // 与 index 插入部分操作相同，使用 map 对象作为参数。
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("updated", DateUtil.date().toDateStr());
        jsonMap.put("reason", "new daily update");
        log.info(DateUtil.date().toDateStr());

        // 此处使用 doc() 方法传入参数，与 DSL 的 API 保持一致.
        updateRequest.doc(jsonMap);

        UpdateResponse updateResponse = restHighLevelClient.update(
                updateRequest, RequestOptions.DEFAULT);
        assertEquals(DocWriteResponse.Result.UPDATED, updateResponse.getResult());
    }

    /**
     * 查询更新方法
     * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-document-update-by-query.html
     *
     * @throws IOException
     */
    @Test
    void updateByQuery() throws IOException {
        UpdateByQueryRequest request = new UpdateByQueryRequest("index_test");
        // 设置搜索条件
        request.setQuery(new TermQueryBuilder("user", "map_user"));

        // 查询最大 doc 数量
        request.setMaxDocs(10);

        // 默认情况下，版本冲突会中止 UpdateByQueryRequest 进程，可以通过在请求体中设置它为 proceed 来计数它们。
        request.setConflicts("proceed");

        // 可以设置批处理大小，默认1000
        request.setBatchSize(2);

        // 支持使用 script 形式修改对象
        request.setScript(
                new Script(
                        ScriptType.INLINE, "painless",
                        "if (ctx._source.user == 'map_user') {ctx._source.message=ctx._source.postDate+' kkk updateByQuery';}",
                        Collections.emptyMap()));

        BulkByScrollResponse bulkResponse = restHighLevelClient.updateByQuery(request, RequestOptions.DEFAULT);

        // 全部 doc 数量
        long totalDocs = bulkResponse.getTotal();
        // 更新的 doc 数量
        long updatedDocs = bulkResponse.getUpdated();
        // 删除的 doc 数量
        long deletedDocs = bulkResponse.getDeleted();
        // 处理的批次数
        long batches = bulkResponse.getBatches();
        // 跳过处理的 doc 数量
        long noops = bulkResponse.getNoops();

        // 失败搜索的信息
        List<ScrollableHitSource.SearchFailure> searchFailures =
                bulkResponse.getSearchFailures();

        Map<String, Object> map = new HashMap<>();
        map.put("totalDocs", totalDocs);
        map.put("updatedDocs", updatedDocs);
        map.put("deletedDocs", deletedDocs);
        map.put("batches", batches);
        map.put("noops", noops);
        map.put("searchFailures", searchFailures);
        log.info(JSONUtil.toJsonStr(map));
    }

    /**
     * 需要白金级授权才能使用JDBC
     */
    @Test
    void jdbcTest() throws SQLException {
        try (Statement statement = connection.createStatement();
             ResultSet results = statement.executeQuery(
                     " SELECT name, age"
                             + "    FROM test_idx"
                             + " ORDER BY age DESC"
                             + " LIMIT 1")) {
            assertTrue(results.next());
            assertEquals("Don Quixote", results.getString(1));
            assertEquals(1072, results.getInt(2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
